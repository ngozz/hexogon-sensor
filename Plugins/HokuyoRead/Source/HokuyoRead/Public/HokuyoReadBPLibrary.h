// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "urg_sensor.h"
#include "urg_utils.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "HokuyoReadBPLibrary.generated.h"

USTRUCT(BlueprintType)
struct FUrgWrapper //This is a wrapper struct that will be used to pass the urg_t type to the blueprint. Can't pass the urg_t type directly to the blueprint.
{
	GENERATED_BODY()

	// Add an instance of the urg_t type
	urg_t Urg;
};

UCLASS(BlueprintType)
class UHokuyoReadBPLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	//open the connection
	UFUNCTION(BlueprintCallable, meta = (ToolTip = "Open Connection with Sensor @param connectionType 0 is Serial. 1 is Ethernet @param ipAddressOrDevice For Serial connection, enter device name like 'COM3'. For Ethernet connection, enter ip address like '192.168.1.1' @param portNumberOrBaudrate For Serial connection, enter baudrate. For Ethernet connection, enter port number"))
	static bool openConnection(UPARAM(ref) FUrgWrapper& UrgWrapper, int32 connectionType = 1, FString ipAddressOrDevice = "192.168.3.202", int32 portNumberOrBaudrate = 10940); 
	//UPARAM(ref) is used to pass a reference to the struct. UE doesn't support references by default using '&'.

	//get scanning distance results
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Get Distance Result (Blocking)", ToolTip = "This will execute on the main thread and freeze your program @param CaptureTimes Number of time it will run. 0 means it will run indefinitely"))
	static bool getDistance(UPARAM(ref) FUrgWrapper& UrgWrapper, int CaptureTimes, TArray<FVector2D>& Points);

	//close the connection
	UFUNCTION(BlueprintCallable)
	static bool closeConnection(UPARAM(ref) FUrgWrapper& UrgWrapper);

	//check the boolean result, if it's false, close the connection
	UFUNCTION(BlueprintCallable)
	static void closeConnectionifFalse(bool Result, UPARAM(ref) FUrgWrapper& UrgWrapper);

	//set scanning degrees
	UFUNCTION(BlueprintCallable)
	static bool setScanningParameters(UPARAM(ref) FUrgWrapper& UrgWrapper);

	//test function
	UFUNCTION(BlueprintPure, meta = (DevelopmentOnly = false, KeyWords = "Swampy", DisplayName = "Test Name", CompactNodeTitle = "Test", ToolTip = "Pew Pew Pew @param value float pew @param ReturnValue2 Hello pew @return return pew"), Category = "HokuyoRead | Sub")
	static bool testFunction(float value, int value2, FString& ReturnValue2);

	//test function
	UFUNCTION(BlueprintCallable)
	static bool hokuyoTest(int value, int& ReturnPew);

	UFUNCTION(BlueprintPure)
	static void PointsToString(const TArray<FVector2D>& Points, FString& StringPoints);

	//async function that calls getDistance. This is used to call getDistance in a non-blocking way.
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Get Distance Result", ToolTip = "Get the Sensor Distance Measurement Result @param CaptureTimes Number of time it will run. 0 means it will run indefinitely"))
	static void getDistanceNonBlocking(UPARAM(ref) FUrgWrapper& UrgWrapper, int CaptureTimes, TArray<FVector2D>& Points);
};


