// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "MotionDetection.generated.h"

UCLASS(BlueprintType)
class UMotionDetection : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	//test function
	UFUNCTION(BlueprintCallable)
	static bool kMeansTest(int value, int& ReturnPew);
};
