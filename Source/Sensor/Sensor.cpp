// Copyright Epic Games, Inc. All Rights Reserved.

#include "Sensor.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Sensor, "Sensor" );
