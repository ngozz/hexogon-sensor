// Fill out your copyright notice in the Description page of Project Settings.


#include "SensorSphere.h"

// Sets default values
ASensorSphere::ASensorSphere()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	SphereMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));

	Scene->SetupAttachment(GetRootComponent());
	SphereMesh->SetupAttachment(Scene);
}

// Called when the game starts or when spawned
void ASensorSphere::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASensorSphere::Tick(float DeltaTime)
{

}


