//#pragma once
//
//#include "CoreMinimal.h"
//#include <vector>
//#include <string>
//
//using namespace std;
//
//class SENSOR_API Point
//{
//private:
//    int pointId, clusterId;
//    int dimensions;
//    vector<double> values;
//
//    vector<double> lineToVec(string& line);
//
//public:
//    Point(int id, string line);
//
//    int getDimensions();
//
//    int getCluster();
//
//    int getID();
//
//    void setCluster(int val);
//
//    double getVal(int pos);
//};
//
//class SENSOR_API Cluster
//{
//private:
//    int clusterId;
//    vector<double> centroid;
//    vector<Point> points;
//
//public:
//    Cluster(int clusterId, Point centroid);
//
//    void addPoint(Point p);
//
//    bool removePoint(int pointId);
//
//    void removeAllPoints();
//
//    int getId();
//
//    Point getPoint(int pos);
//
//    int getSize();
//
//    double getCentroidByPos(int pos) const;
//
//    void setCentroidByPos(int pos, double val);
//};
//
//class SENSOR_API KMeans
//{
//private:
//    int K, iters, dimensions, total_points;
//    vector<Cluster> clusters;
//
//    void clearClusters();
//
//    int getNearestClusterId(Point point);
//
//public:
//    KMeans(int K, int iterations);
//
//    void run(vector<Point>& all_points);
//
//    const vector<Cluster>& getClusters() const;
//};
