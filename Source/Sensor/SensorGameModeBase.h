// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SensorGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SENSOR_API ASensorGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
